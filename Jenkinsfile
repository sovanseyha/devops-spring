pipeline {
    agent any

    tools {
        maven 'maven'
        jdk 'jdk'
    }

    environment {
        DOCKER_REGISTRY = 'sovanseyha'
        IMAGE_NAME = 'spring-app'
        CONTAINER_NAME = 'spring-container'
        TELEGRAM_BOT_TOKEN = credentials('telegram-token')
        TELEGRAM_CHAT_ID = credentials('chat-id')
        BUILD_INFO = "${currentBuild.number}"
        COMMITTER = sh(script: 'git log -1 --pretty=format:%an', returnStdout: true).trim()
        BRANCH = sh(script: 'git rev-parse --abbrev-ref HEAD', returnStdout: true).trim()

        IMAGE_TAG = "${DOCKER_REGISTRY}/${IMAGE_NAME}:${BUILD_INFO}"
        BUILD_URL = "${env.BUILD_URL}"
    }

    stages {
        stage('Notify Start') {
            steps {
                script {
                    echo "🚀 Pipeline Started: ${env.JOB_NAME}"
                    sendTelegramMessage("🚀 Pipeline Started:\nJob Name: ${env.JOB_NAME}\nVersion: ${BUILD_INFO}\nCommitter: ${COMMITTER}\nBranch: ${BRANCH}")
                }
            }
        }

        stage('Maven Build') {
            steps {
                script {
                    try {
                        sh 'mvn clean package'
                    } catch (Exception e) {
                        currentBuild.result = 'FAILURE'
                        def errorMessage = "❌ Build stage failed:\n${e.getMessage()}\nVersion: ${BUILD_INFO}\nCommitter: ${COMMITTER}\nBranch: ${BRANCH}\nConsole Output: ${env.BUILD_URL}console"
                        sendTelegramMessage(errorMessage)
                        error(errorMessage)
                    }
                }
            }
        }

        stage('Check for Existing Container') {
            steps {
                script {
                    try {
                        def containerId = sh(script: "docker ps -a --filter name=${env.CONTAINER_NAME} -q", returnStdout: true).trim()
                        echo "Container ID is ${containerId}"
                        if (containerId) {
                            sh "docker stop ${containerId}"
                            sh "docker rm ${containerId}"
                        }
                    } catch (Exception e) {
                        currentBuild.result = 'FAILURE'
                        sendTelegramMessage("❌ Check for Existing Container stage failed: ${e.message}\nVersion: ${BUILD_INFO}\nCommitter: ${COMMITTER}\nBranch: ${BRANCH}")
                        error("Check for Existing Container stage failed: ${e.message}")
                    }
                }
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    try {
                        def buildNumber = currentBuild.number
                        def imageTag = "${IMAGE_NAME}:${buildNumber}"
                        sh "docker build -t ${DOCKER_REGISTRY}/${imageTag} ."

                        withCredentials([usernamePassword(credentialsId: 'docker-hub-cred',
                                passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                            sh "echo \$PASS | docker login -u \$USER --password-stdin"
                            sh "docker push ${DOCKER_REGISTRY}/${imageTag}"
                        }
                    } catch (Exception e) {
                        currentBuild.result = 'FAILURE'
                        sendTelegramMessage("❌ Build Image stage failed: ${e.message}\nVersion: ${BUILD_INFO}\nCommitter: ${COMMITTER}\nBranch: ${BRANCH}")
                        error("Build Image stage failed: ${e.message}")
                    }
                }
            }
        }

        // stage('SonarQube analysis') {
        //     environment {
        //         SCANNER_HOME = tool 'sonar'
        //     }
        //     steps {
        //         withSonarQubeEnv(credentialsId: 'sonar-token', installationName: 'sonar') {
        //             sh '''$SCANNER_HOME/bin/sonar-scanner \
        //             -Dsonar.projectKey=spring-app \
        //             -Dsonar.projectName=spring-app \
        //             -Dsonar.sources=src/ \
        //             -Dsonar.java.binaries=target/classes/ \
        //             -Dsonar.exclusions=src/test/java/****/*.java \
        //             -Dsonar.java.libraries=/var/lib/jenkins/.m2/**/*.jar \
        //             -Dsonar.projectVersion=${BUILD_NUMBER}-${GIT_COMMIT_SHORT}'''
        //         }
        //     }
        // }

        // stage('Trivy Scanner') {
        //     steps {
        //         script {
        //             sh "trivy image ${IMAGE_TAG} --format list -o formatted_trivy.txt"

        //             // Send the formatted content to Telegram
        //             sendTelegramMessage("🔍 Trivy Scan Results:\n${BUILD_INFO} - ${COMMITTER} - ${BRANCH}\nView the detailed report: ${BUILD_URL}artifact/formatted_trivy.txt")
        //         }
        //     }
        // }


        stage('Trivy Scanner') {
            steps {
                script {
                    def IMAGE_TAG = "${DOCKER_REGISTRY}/${IMAGE_NAME}:${BUILD_INFO}"
                    // def FILE_PATH = "${WORKSPACE}/trivy_${IMAGE_NAME}.txt"

                    // sh """
                    //     trivy image ${IMAGE_TAG} \
                    //     --scanners vuln,config,secret --format template --template "@contrib/html.tpl" | tee ${FILE_PATH}
                    // """

                    sh "trivy image -f table ${IMAGE_NAME} > ${IMAGE_NAME}.txt"

                    // Print the content of the Trivy report file
                    sh "cat ${FILE_PATH}"

                    // Display file size
                    sh "ls -l ${FILE_PATH}"

                    // Verify file type
                    sh "file ${FILE_PATH}"

                    // Send the Trivy report file to Telegram
                    sh """
                        curl -F chat_id=${TELEGRAM_CHAT_ID} \
                        -F document=@${IMAGE_TAG}.txt \
                        -F caption='🔍 Trivy Scan Results: ${BUILD_INFO} - ${COMMITTER} - ${BRANCH}' \
                        -H "Content-Type:multipart/form-data" \
                        https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendDocument
                    """
                }
            }
        }



    // post {
    //     success {
    //         sendTelegramMessage("✅ All stages succeeded\nVersion: ${BUILD_INFO}\nCommitter: ${COMMITTER}\nBranch: ${BRANCH}")
    //         emailext body: "<html><body><b>✅ All stages succeeded</b><br/>Version: ${BUILD_INFO}<br/>Committer: ${COMMITTER}<br/>Branch: ${BRANCH}<br/>Check console output at <a href='${BUILD_URL}'>${BUILD_URL}</a> to view the results.</body></html>",
    //             subject: "${env.JOB_NAME} - Build #${env.BUILD_NUMBER} - ${currentBuild.currentResult}",
    //             to: "yan.sovanseyha@gmail.com",
    //             mimeType: 'text/html'
    //     }
    //     failure {
    //         emailext body: "<html><body><b>❌ Pipeline failed</b><br/>Version: ${BUILD_INFO}<br/>Committer: ${COMMITTER}<br/>Branch: ${BRANCH}<br/>Check console output at <a href='${BUILD_URL}'>${BUILD_URL}</a> to view the results.</body></html>",
    //             subject: "${env.JOB_NAME} - Build #${env.BUILD_NUMBER} - ${currentBuild.currentResult}",
    //             to: "yan.sovanseyha@gmail.com",
    //             mimeType: 'text/html'
    //     }
    // }
    }
}

def sendTelegramMessage(message) {
    script {
        sh """
            curl -s -X POST https://api.telegram.org/bot\${TELEGRAM_BOT_TOKEN}/sendMessage -d chat_id=\${TELEGRAM_CHAT_ID} -d parse_mode="HTML" -d text="${message}"
        """
    }
}
